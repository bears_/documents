<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * FileUploader
 */
class FileUploader
{
    /**
     * @var string
     */
    private $dir;

    /**
     * @param string $dir
     */
    public function __construct(string $dir)
    {
        $this->dir = $dir;
    }

    /**
     * @return string
     */
    public function getDir(): string
    {
        return $this->dir;
    }

    /**
     * @param  UploadedFile $file
     * @return string
     */
    public function upload(UploadedFile $file): string
    {
        $fileName = $file->getClientOriginalName();

        if (file_exists($this->getDir() . $fileName)) {
            $pathParts = pathinfo($this->getDir() . $fileName);
            while (file_exists($this->getDir() . $fileName)) {
                $fileName = sprintf('%s%s.%s', $pathParts['filename'], random_int(1, 100), $pathParts['extension']);
            }
        }

        $file->move($this->getDir(), $fileName);

        return $fileName;
    }
}
