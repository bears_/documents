<?php

namespace AppBundle\Twig;

use AppBundle\Extensions\ImageManager;

class AppExtension extends \Twig_Extension
{
    /**
     * @var string
     */
    private $uploadsDir;

    /**
     * @var string
     */
    private $dir;

    /**
     * @param string $uploadsDir
     * @param string $dir
     */
    public function __construct(string $uploadsDir, string $dir)
    {
        $this->uploadsDir = $uploadsDir;
        $this->dir = $dir;
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        return [
            new \Twig_SimpleFilter('size', [$this, 'size']),
            new \Twig_SimpleFilter('fileType', [$this, 'fileType']),
            new \Twig_SimpleFilter('file', [$this, 'file']),
        ];
    }

    /**
     * @param  string $image
     * @return string
     */
    public function size(string $size): string
    {
        if ($size < 1024) {
            return round($size) . ' kb';
        }

        return round($size / 1024) . ' mb';
    }

    /**
     * @param  string $file
     * @return string
     */
    public function fileType(string $file): string
    {
        $file = $this->uploadsDir . $file;

        $images = ['jpg', 'jpeg', 'png', 'gif'];

        $ex = pathinfo($file, PATHINFO_EXTENSION);

        if (in_array($ex, $images)) {
            return 'Изображение';
        }

        if ($ex === 'pdf') {
            return 'PDF документ';
        }

        if (in_array($ex, ['doc', 'docx', 'rtf'])) {
            return 'MS Word документ';
        }

        return $ex;
    }

    /**
     * @param  string $file
     * @return string
     */
    public function file(string $file): string
    {
        return $this->dir . $file;;
    }
}
