<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Attachment;

class AttachmentController extends Controller
{
    /**
     * @Route("/attachment/remove/{id}", name="app_attachment_remove")
     */
    public function addAction(Attachment $attachment)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($attachment);
        $em->flush();

        return $this->json([]);
    }
}
