<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\{Attachment, Document};
use AppBundle\Form\DocumentType;

class DocumentController extends Controller
{
    /**
     * @Route("/", name="app_documents")
     */
    public function indexAction(Request $request)
    {
        $documents = $this->getDoctrine()
            ->getRepository(Document::class)
            ->findAll();

        return $this->render('index.html.twig', [
            'documents' => $documents
        ]);
    }

    /**
     * @Route("/{id}", name="app_document")
     */
    public function viewAction(Document $document)
    {
        return $this->render('document.html.twig', [
            'document' => $document
        ]);
    }

    /**
     * @Route("/document/add", name="app_document_add")
     */
    public function addAction(Request $request)
    {
        $ip = $request->server->get('REMOTE_ADDR');

        $em = $this->getDoctrine()->getManager();

        $document = $em->getRepository(Document::class)->findOneBy(['authorIp' => $ip]);
        if (null === $document) {
            $document = new Document();
            $document->setAuthorIp($ip);
        }

        $em->persist($document);
        $em->flush();

        return $this->redirectToRoute('app_document_edit', ['id' => $document->getId()]);
    }

    /**
     * @Route("/document/edit/{id}", name="app_document_edit")
     */
    public function editAction(Request $request, Document $document)
    {
        $form = $this->createForm(DocumentType::class, $document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $document->setAuthorIp(null);

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('app_documents');
        }

        return $this->render('form.html.twig', [
            'document' => $document,
            'form'     => $form->createView()
        ]);
    }

    /**
     * @Route("/document/remove/{id}", name="app_document_remove")
     */
    public function removeAction(Document $document)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($document);
        $em->flush();

        return $this->json([]);
    }

    /**
     * @Route("/document/upload/{id}", name="app_upload")
     */
    public function uploadAction(Request $request, Document $document)
    {
        $attachment = new Attachment();
        $attachment
            ->setFile($request->files->get('file'))
            ->setDocument($document);

        $em = $this->getDoctrine()->getManager();
        $em->persist($attachment);
        $em->flush();

        $attachment = $document->getAttachments();

        return $this->render('_files.html.twig', [
            'attachments' => $attachment
        ]);
    }
}
