<?php

namespace AppBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\HttpFoundation\File\{UploadedFile, File};
use AppBundle\Service\FileUploader;
use AppBundle\Entity\Attachment;

/**
 * EntitySubscriber
 */
class EntitySubscriber implements EventSubscriber
{
    /**
     * @var FileUploader
     */
    private $uploader;

    /**
     * Constructor
     */
    public function __construct(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    /**
     * @return array
     */
    public function getSubscribedEvents(): array
    {
        return ['preUpdate', 'prePersist', 'postLoad', 'postRemove'];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args): void
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof Attachment) {
            return;
        }

        if ($fileName = $entity->getName()) {
            $entity->setFile(new File($this->uploader->getDir().$fileName));
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof Attachment) {
            return;
        }

        if ($entity->getName()) {
            unlink($this->uploader->getDir().$entity->getName());
        }
    }

    /**
     * @param $entity
     */
    private function uploadFile($entity): void
    {
        if (!$entity instanceof Attachment) {
            return;
        }

        $file = $entity->getFile();

        // only upload new files
        if ($file instanceof UploadedFile) {
            if ($entity->getName()) {
                unlink($this->uploader->getDir().$entity->getName());
            }

            $fileName = $this->uploader->upload($file);
            $entity->setName($fileName);

            $size = filesize($this->uploader->getDir() . $fileName) / 1024;
            $entity->setSize($size);
        }
    }
}
