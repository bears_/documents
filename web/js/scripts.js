jQuery(function($) {

    if ($('#addFileContainer').length) {
        var config = {
            runtimes: 'html5',
            browse_button: 'addFile',
            container: document.getElementById('addFileContainer'),
            url: document.getElementById('addFile').getAttribute('data-url'),
            file_data_name: 'file',
            multi_selection: true,
            filters: [{title: 'Image files', extensions: 'doc,docx,rtf,pdf,jpg,jpeg,png,gif'}],
            init: {
                FilesAdded: function(up, files) {
                    up.start();
                },
                FileUploaded: function(up, file, response) {
                    $('.files').html(response.response);
                }
            }
        };
        var uploader = new plupload.Uploader(config);
        uploader.init();
    }

    $('.files, .documents').on('click', '.remove', function(e) {
        e.preventDefault();

        var link = $(this);

        $.post(link.attr('href'), function(response) {
            link.closest('tr').fadeOut(function() {
                $(this).remove();
            });
        });
    });
});